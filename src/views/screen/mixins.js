/*
 * @Author: gyp
 * @Date: 2020-05-08 18:20:13
 * @LastEditors: gyp
 * @LastEditTime: 2020-09-09 11:22:38
 * @Description: 大屏的属性和方法
 * @FilePath: \sy_kjxc_web\src\views\screen\mixins.js
 */
import customMapConfig from '@/assets/json/custom_map_config.json';
import { formateTime } from '@/utils/common.js';
import Queue from 'queue';
import _ from 'lodash';
const basicScreen = {
  data () {
    // 中间顶部的统计数据
    const headStaData = [
      {
        icon: 'component', // 图标
        title: '当日警情数', // 名称
        value: 0 // 数值
      },
      {
        icon: 'dashboard',
        title: '平均处警时长',
        value: 0
      },
      {
        icon: 'policenum',
        title: '实时警情数',
        value: 0
      },
      {
        icon: 'example',
        title: '实时线路考勤',
        value: 0
      },
      {
        icon: 'chart',
        title: '实时人员考勤',
        value: 0
      }
    ];
    return {
      headStaData: headStaData, // 中间顶部的统计数据
      BMap: null, // 百度地图类
      map: null, // 百度地图实例
      cityName: '邵阳', // 地图城市
      center: { lng: 111.48, lat: 27.229 }, // 地图中心点
      zoom: 14, // 地图初始化缩放级别
      mapStyle: {
        // 地图自定义样式
        styleJson: customMapConfig
      },
      platformList: [], // 快警平台信息
      carnavData: [
        { name: '全部', value: '0' },
        { name: '平台车', value: '1' },
        { name: '处警车', value: '2' },
        { name: '摩托车', value: '3' }
      ], // 巡检车辆的导航
      curnavIndex: 0, // 当前选中的巡检车辆的索引,
      patrolrankList: [
        {
          parentName: '双清分局',
          platformName: '洞口县 1号平台',
          distance: 35
        },
        {
          parentName: '双清分局',
          platformName: '洞口县 1号平台',
          distance: 35
        },
        {
          parentName: null,
          platformName: '洞口县 1号平台',
          distance: 35
        }
      ], // 巡逻排名
      realtimeAlertList: [
        {
          jjzzjgmc: '北塔汽车北站2号平台',
          bjnr: '其朋友在邵阳大饭店被人殴打',
          BJSJ: '08:13:04',
          jqlx: 1
        },
        {
          jjzzjgmc: '北塔汽车北站2号平台',
          bjnr: '在资源菜市场西头有个小孩迷路了',
          BJSJ: '08:13:03',
          jqlx: 1
        },
        {
          jjzzjgmc: '北塔江北广场1号平台',
          bjnr: '在西湖春天售楼部，有人闹事。',
          BJSJ: '07:59:59',
          jqlx: 2
        },
        {
          jjzzjgmc: '北塔汽车北站2号平台',
          bjnr: '在北站商业小区1栋有邻居打其家人',
          BJSJ: '07:59:45',
          jqlx: 3
        },
        {
          jjzzjgmc: '大祥城南公园5号平台',
          bjnr: '雪峰路平台请求增援',
          BJSJ: '07:59:30',
          jqlx: 3
        },
        {
          jjzzjgmc: '大祥雪峰南路3号平台',
          bjnr: '其在3路公交车上被扒了3000元现金及银行卡。',
          BJSJ: '07:59:15',
          jqlx: 2
        },
        {
          jjzzjgmc: '大祥火车南站4号平台',
          bjnr: '在火车南站公交车站被拔手机一台',
          BJSJ: '07:58:00',
          jqlx: 2
        },
        {
          jjzzjgmc: '大祥城南公园5号平台',
          bjnr: '报警人称老人在城南公园走失',
          BJSJ: '07:58:45',
          jqlx: 1
        }
      ], // 实时警情
      patrolcarList: [], // 当前类别的巡检车辆
      platformId: '', // 当前平台id
      platformTitle: '', // 当前平台名称
      policeVisible: false, // 快警-弹出框可见性
      dutyleaderVisible: false, // 大队值班领导-弹出框可见性
      userdetailVisible: false, // 用户详情-弹出框可见性
      situationVisible: false, // 警情-弹出框可见性
      dealsituaVisible: false, // 处警-弹出框可见性
      realtimealertnumVisible: false, // 实时警情数-弹出框可见性
      punchVisible: false, // 未打卡列表-弹出层可见性
      datacheckVisible: false, // 数据考核-弹出框可见性
      recordlistVisible: false, // 打卡记录-弹出框可见性
      patroltaskVisible: false, // 巡逻任务-弹出框可见性
      maptrackVisible: false, // 地图轨迹-弹出框可见性
      currentUserId: '', // 当前选中用户的id
      currentUserName: '', // 当前选中用户的名称
      markers: [], // 车辆点位数据
      actualMarkers: [], // 实际该显示的车辆
      showMarkers: [], // 真正在可视区域显示的车辆点位数据
      polygons: [], // 全部网格数组
      showPolygons: [], // 显示的网格数组
      labels: [], // 网格标注
      showLabels: [], // 显示的网格标注
      videoStyle: {
        height: '100%',
        width: '100%'
      },
      lastInfoBox: null, // 弹出框窗口
      showView: false, // 视频播放可见性
      videoDevid: '', // 视频播放的设备id
      videoName: '', // 视频播放标题
      viewUrlMap: {}, // 存储视频的对象
      queue: Queue(), // 用来存车辆数据的队列
      timer: null, // 实时点位
      todayDate: '', // 今天日期
      interStatus: 'inside', // inside:内网  outside:外网
      currentDevId: '', // 车辆设备号
      currentDevType: '', // 车辆类型
      currentDevName: '', // 车辆设备名称
      carTnter: null, // 车辆的定位定时器
      realtimeAlertInter: null, // 实时警情5分钟请求一次
      layerboxVisible: false, // 控制图层容器-可见性
      mapFullscreen: false, // 地图全屏-可见性
      platformCar: true, // 平台车-可见性
      policeCar: true, // 警车-可见性
      motorcycle: true, // 摩托车-可见性
      jurisdiction: true, // 辖区-可见性
      inputCar: '', // 车辆搜索关键字
      chartBackOne: false, // 统计图一是否是二级图标，有返回按钮
      chartBack: 'first', // first 一级 second/third 二/三级
      oneChartBar: '', // 柱状图一实例
      twoChartBar: '', // 柱状图二实例
      inputcarPatrol: '', // 巡检车辆搜索
      chartTwoYear: '', // 柱状图二年份记录
      monthArray: { '一月': 1, '二月': 2, '三月': 3, '四月': 4, '五月': 5, '六月': 6, '七月': 7, '八月': 8, '九月': 9, '十月': 10, '十一月': 11, '十二月': 12 },
      localUrl: 'http://localhost:8015',
      secondYear: '', // 统计图一下载年
      secondMonth: '', // 统计图一下载月
      thirdYear: '', // 统计图二下载年
      thirdMonth: '', // 统计图二下载月
      thirdType: '', // 统计图二下载类型
      thirdName: '' // 统计图二下载标题
    };
  },
  watch: {
    zoom (newVal) {
      if (newVal < 14) {
        this.showLabels = [];
        if (newVal < 12) {
          this.showPolygons = [];
        } else {
          // 辖区
          this.getJurisdiction();
        }
      } else {
        if (this.platformTitle) {
          let parentName = this.platformTitle.split('-')[0];
          let name = this.platformTitle.split('-')[1];
          let pName = parentName.slice(0, parentName.length - 1);
          this.showLabels = this.labels.filter(item => {
            let labelName = item.name;
            return labelName.includes(pName) && labelName.includes(name);
          });
        } else {
          this.showLabels = _.cloneDeep(this.labels);
        }
      }
    }
  },
  methods: {
    // 地图组件渲染完毕时触发
    handler ({ BMap, map }) {
      this.BMap = BMap;
      this.map = map;
      // this.mapInit();
      this.getPoliceCarInit();
      // let markerClusterer = new BMapLib.MarkerClusterer(this.map);
      // markerClusterer.setGridSize(1);
      // 设置地图的边界
      let bdary = new BMap.Boundary();
      bdary.get(this.cityName, rs => {
        let plyInner = new BMap.Polyline(rs.boundaries[0], {
          strokeWeight: 3,
          strokeColor: '#5ab1ef',
          fillColor: ''
        }); // 目标地区
        this.map.addOverlay(plyInner); // 添加覆盖物
        plyInner.disableMassClear(); // 禁止移除
        // 限制展示范围
        // let b = new BMap.Bounds(
        //   new BMap.Point(108.699315, 24.926306),
        //   new BMap.Point(113.114666, 30.735636)
        // );
        // try {
        //   BMapLib.AreaRestriction.setBounds(map, b);
        // } catch (e) {
        //   alert(e);
        // }
      });
    },
    /**
     * 地图初始化
     */
    mapInit () {
      const map = this.map;
      // 设置地图的边界
      let bdary = new BMap.Boundary();
      bdary.get(this.cityName, rs => {
        let EN_JW = '180, 90;'; // 东北角
        let NW_JW = '-180,  90;'; // 西北角
        let WS_JW = '-180, -90;'; // 西南角
        let SE_JW = '180, -90;'; // 东南角
        let plyOut = new BMap.Polygon(
          rs.boundaries[0] + SE_JW + SE_JW + WS_JW + NW_JW + EN_JW + SE_JW,
          {
            strokeColor: 'none',
            fillOpacity: 1,
            strokeOpacity: 0.5,
            fillColor: '#0C1C31'
          }
        ); // 目标地区外
        let plyInner = new BMap.Polyline(rs.boundaries[0], {
          strokeWeight: 2,
          strokeColor: '#5ab1ef',
          fillColor: ''
        }); // 目标地区
        map.addOverlay(plyOut); // 添加覆盖物
        map.addOverlay(plyInner); // 添加覆盖物
        plyOut.disableMassClear(); // 禁止移除
        plyInner.disableMassClear(); // 禁止移除
        // 限制展示范围
        let b = new BMap.Bounds(
          new BMap.Point(108.699315, 25.926306),
          new BMap.Point(113.114666, 27.735636)
        );
        try {
          BMapLib.AreaRestriction.setBounds(map, b);
        } catch (e) {
          alert(e);
        }
      });
    },
    /**
     * 老接口-获取全部数据
     */
    getPoliceCarInit () {
      this.$api.screen.getPoliceCarInit().then(res => {
        // axios.get('http://218.76.207.66:8181/api/getPoliceCarInit').then(res => {
        if (this.interStatus === 'outside') {
          this.getAllGrid(res.data.data);
        }
        this.getAllPoint(res.data.data);
      });
    },
    /**
     * 设置地图上车辆的平台网格
     * @param {Array} data 网格数据
     */
    getAllGrid (data) {
      let gridArray = []; // 格式化后的网格数据
      let coordArray = []; // 未格式化的网格数据
      let labelArray = []; // 未格式化的网格标签
      data.forEach(item => {
        let objGrid = {
          name: item.parentName + item.name,
          coordList: item.coordList
        };
        coordArray.push(objGrid);
        if (item.conrdinate) {
          let lng = 0;
          let lat = 0;
          if (item.conrdinate.split(',')) {
            lng = item.conrdinate.split(',')[0];
            lat = item.conrdinate.split(',')[1];
          } // 格式化
          let objLabel = {
            name: item.parentName + item.name,
            conrdinate: lng && lat ? { lng, lat } : ''
          };
          labelArray.push(objLabel);
        }
      });
      this.labels = labelArray;
      this.showLabels = labelArray;
      coordArray.map(each => {
        const { name, coordList } = each;
        let tempArray = []; // 一个区域网格
        if (coordList.length) {
          // 数组有坐标点才进行计算
          coordList.forEach(item => {
            // 一个分局可能包含多个分区
            let arr = item[0].split(';');
            if (arr.length) {
              // 有坐标点才进行计算
              arr.forEach(element => {
                let eleCoord = element.split(',');
                let obj = {
                  lng: eleCoord[0],
                  lat: eleCoord[1]
                };
                tempArray.push(obj);
              });
            }
          });
        }
        if (tempArray.length) {
          // 有数据才加入网格数据组
          gridArray.push({
            name,
            coordList: tempArray
          });
        }
      });
      this.polygons = gridArray;
      this.showPolygons = _.cloneDeep(this.polygons);
    },
    /**
     * 设置地图上车辆的点位
     * @param {Array} data 车辆数据
     */
    getAllPoint (data) {
      let pointArray = [];
      data.forEach(every => {
        const { parentName, name, leader, resourceList } = every;
        if (resourceList.length) {
          // 有点位
          resourceList.forEach(item => {
            const { type, jsonText } = item;
            let lon = 0;
            let lat = 0;
            if (jsonText && jsonText.lon && jsonText.lat) {
              lon = jsonText.lon;
              lat = jsonText.lat;
            } else {
              lon = item.lon;
              lat = item.lat;
            }
            // if (lon && lat) {
            // 有经纬度
            let obj = {
              ...item,
              lng: lon,
              lat,
              wholeName: parentName + name,
              parentName,
              name: name,
              leader,
              isLocked: false, // 是否被锁定
              icon: require('../../assets/icon/car' + type + '.png'),
              iconLock: require('../../assets/icon/car' + type + '_lock.png')
            };
            delete obj.lon;
            pointArray.push(obj);
            // }
          });
        }
      });
      this.markers = pointArray;
      this.actualMarkers = pointArray;
      this.showMarkers = this.queryInRect(this.markers);
      this.patrolcarList = pointArray;
    },
    /**
     * 车辆点击弹出框
     * @param {Object} marker 点信息
     * @param {Boolean} status 查看监控状态 true: 直接打开监控，不弹出窗口
     */
    onMarkerClick (marker, status) {
      this.showView = false;
      this.videoDevid = '';
      this.videoName = '';
      const {
        lng,
        lat,
        parentName,
        name,
        leader,
        carCode,
        type,
        devid,
        jsonText,
        videoList
      } = marker;
      let mileage = 0;
      let speed = 0;
      if (jsonText) {
        mileage = jsonText.mileage ? jsonText.mileage : 0;
        speed = jsonText.speed ? jsonText.speed : 0;
      }
      let myWindow = [
        '<div class="myInfobox" id = "infoWindow">' +
        '<div class="top-left"></div>' +
        '<div class="top-right"></div>' +
        '<div class="bottom-left"></div>' +
        '<div class="bottom-right"></div>' +
        '<div class="top">' +
        '<div class="pName">' +
        parentName +
        '</div>' +
        '<div class="sName">' +
        name +
        '</div>' +
        '</div>' +
        '<div class="center">' +
        '<div class="carName">' +
        carCode +
        '</div>' +
        '<div class="leaderName">' +
        leader +
        '</div>' +
        '</div>'
      ];
      switch (type) {
        case '1':
          myWindow[0] += '<div style = "border:1px solid #5ab1ef;height:73%">';
          myWindow[0] +=
            '<div style="display:flex;flex-direction: row;text-align:left;color: #ffffff;font-size: 14px;margin: 0.05rem 0.1rem;">' +
            '<div style=\'color: #5ab1ef;\'>装备清单</div>' +
            '</div>' +
            '<div style="display:flex;flex-direction: row;color: #ffffff;font-size: 14px;margin: 0.05rem 0.1rem;">' +
            '<div style=\'width: 30%\'>手枪:2</div><div style=\'width: 38%\'>防弹头盔:5 </div><div style=\'width: 34%\'>警戒带:2</div></div>' +
            '<div style="display:flex;flex-direction: row;justify-content:space-between;color: #ffffff;font-size: 14px;margin: 0.05rem 0.1rem;">' +
            '<div style=\'width: 30%\'>盾牌:4</div><div style=\'width: 38%\'>单警装备:10</div><div style=\'width: 34%\'>防弹衣:5</div>' +
            '</div>' +
            '<div style="display:flex;flex-direction: row;justify-content:space-between;color: #ffffff;font-size: 14px;margin: 0.05rem 0.1rem;">' +
            '<div style=\'width: 30%\'>阻车器:2</div><div style=\'width: 38%\'>电子抓捕器:2</div><div style=\'width: 34%\'>执法记录仪:6</div></div>' +
            '<div style="display:flex;flex-direction: row;justify-content:space-between;color: #ffffff;font-size: 14px;margin: 0.05rem 0.1rem;">' +
            '<div style=\'width: 30%\'>约束带:4</div><div style=\'width: 38%\'>停车示意牌:5</div><div style=\'width: 34%\'>反光背心:8</div>' +
            '</div>' +
            '<div style="display:flex;flex-direction: row;justify-content:space-around;color: #ffffff;font-size: 14px;margin: 0.05rem 0.1rem;">' +
            '<div style=\'width: 60%\'>处警车350兆车载台:3</div><div style=\'width: 40%\'>350兆对讲机:7</div>' +
            '</div>';
          break;
        case '2':
        case '3':
          myWindow[0] += '<div style = "border:1px solid #5ab1ef;height:73%">';
          myWindow[0] +=
            '<div style="display:flex;flex-direction: row;text-align:left;color: #ffffff;font-size: 14px;margin: 0.3rem 0.1rem;">' +
            '<div style=\'width: 100%\' id = \'male' +
            devid +
            '\'>今日巡逻里程：' +
            mileage +
            '公里</div>' +
            '</div>' +
            '<div style="display:flex;flex-direction: row;text-align:left;color: #ffffff;font-size: 14px;margin: 0.3rem 0.1rem;">' +
            '<div style=\'width: 100%\' id = \'speed' +
            devid +
            '\'>速度：' +
            (speed ? speed.toFixed(2) : 0) +
            '公里/小时</div>' +
            '</div>';
          break;
        default:
          break;
      }
      if (videoList !== null && videoList.length > 0) {
        this.viewUrlMap[devid] = videoList;
        myWindow[0] +=
          '<div style="display:flex;flex-direction: row;justify-content:space-around;color: #25f3e6;font-size: 15px;margin: 0.1rem;font-weight: bold">' +
          '        <div style="cursor:pointer;text-align: left;font-weight:bold;" onclick="tapclick(\'' +
          devid +
          '\',\'' +
          carCode +
          '\')">查看监控</div>' +
          '      </div>' +
          '    </div>';
      } else {
        myWindow[0] += ' </div>   </div>';
      }
      if (status) {
        // 车辆列表查看监控
        window.tapclick(devid, carCode);
      } else {
        // 地图上点击marker
        if (this.lastInfoBox) {
          this.lastInfoBox.close();
        }
        let infoBox = new BMapLib.InfoBox(this.map, myWindow, {
          boxStyle: {
            background: 'transparent',
            width: '3.50rem',
            height: '3rem'
          },
          offset: new this.BMap.Size(0, 35),
          closeIconMargin: '1px 1px 0 0',
          enableAutoPan: true,
          closeIconUrl: require('../../assets/icon/close.png')
        });
        this.lastInfoBox = infoBox;
        let point = new this.BMap.Point(lng, lat);
        this.lastInfoBox.open(new this.BMap.Marker(point));
      }
    },
    /**
     * 右键锁定地图功能,更改为了列表中的按钮
     * @param {Object} marker 点信息
     */
    onMarkerRight (marker, type) {
      const { id, isLocked } = marker;
      if (isLocked) {
        // 解锁
        this.setFullmap();
        if (type === 'track' && this.carTnter) {
          clearInterval(this.carTnter);
        }
      } else {
        // 锁定
        this.markers.map(item => {
          item.isLocked = false;
        });
        this.showMarkers = this.markers.filter(item => item.id === id);
        console.log(this.showMarkers)
        this.showMarkers[0].isLocked = true;
        const { lng, lat } = this.showMarkers[0];
        if (lng && lat) {
          this.panTo(lng, lat);
          setTimeout(() => {
            this.center = {
              lng,
              lat
            };
          }, 1000);
        } else {
          this.$message.info('此车辆暂无位置信息');
        }
        // 定时执行车辆定位运动
        if (type === 'track') {
          this.carTnter = setInterval(() => {
            let nowMarker = this.markers.filter(item => item.id === id)[0];
            setTimeout(() => {
              this.center = { lng: nowMarker.lng, lat: nowMarker.lat };
            }, 1000);
          }, 60000);
        }
      }
    },
    panTo (lng, lat) {
      let point = new BMap.Point(lng, lat);
      this.map.panTo(point);
      this.map.centerAndZoom(point);
    },
    /**
     * 巡检车辆切换标签
     * @param {String} val tab标签对应值
     */
    changeCarList (val = this.curnavIndex + '') {
      if (val) {
        this.curnavIndex = parseInt(val);
      }
      let cloneMarkers = this.markers;
      // 关键字过滤
      if (this.inputcarPatrol) {
        cloneMarkers = this.markers.filter(item => {
          let inputcarPatrol = this.inputcarPatrol;
          let carCode = item.carCode;
          let deptName = item.deptName;
          return (
            (carCode ? carCode.includes(inputcarPatrol) : '') ||
            (deptName ? deptName.includes(inputcarPatrol) : '')
          );
        });
      }
      if (val === '0') {
        // 全部
        this.patrolcarList = cloneMarkers;
      } else {
        this.patrolcarList = cloneMarkers.filter(item => item.type === val);
      }
    },
    /** 巡检车辆搜索 */
    getCarPatrol () {
      if (this.inputcarPatrol) {
        this.changeCarList();
      } else {
        this.$message.info('请输入查询的车牌号或者平台车名');
      }
    },
    /**
     * 监控车辆
     * @param {Object} row 一行的值
     */
    monitorCar (row) {
      this.onMarkerClick(row, true);
    },
    initWebSocket () {
      // 初始化weosocket
      let wsuri = 'ws://218.76.207.66:8181/quickWebSocketServer.action';
      this.websock = new WebSocket(wsuri);
      this.websock.onopen = this.websocketonopen;
      this.websock.onerror = this.websocketonerror;
      this.websock.onmessage = this.websocketonmessage;
      this.websock.onclose = this.websocketclose;
    },
    videoClose () {
      this.showView = false;
    },
    websocketonopen () {
      console.log('WebSocket连接成功');
    },
    websocketonerror (e) {
      // 错误
      console.log('WebSocket连接发生错误' + e.data);
    },
    websocketonmessage (e) {
      // 数据接收
      if (e.data !== undefined) {
        this.queue.push(JSON.parse(e.data));
      }
    },
    websocketsend (agentData) {
      // 数据发送
      this.websock.send(agentData);
    },
    websocketclose: function (e) {
      // 关闭
      console.log('connection closed (' + e.code + ')');
    },
    /**
     * 实时车辆位置
     */
    setTimer () {
      this.timer = setInterval(() => {
        let queue = this.queue;
        if (queue.length > 0) {
          let len = queue.length;
          if (len > 30) len = 30;
          for (let i = 0; i < len; i++) {
            let obj = queue.shift();
            if (obj) {
              const { deviceId, lon, lat, speed } = obj;
              this.markers.map(item => {
                if (item.devid === deviceId) {
                  item.lng = lon;
                  item.lat = lat;
                }
                if (deviceId === '18274323934') {
                  console.log(item.speed);
                }
                if (item.jsonText) {
                  item.jsonText.speed = speed;
                } else {
                  item.jsonText = {
                    speed
                  };
                }
              });
            }
          }
        }
      }, 1000);
    },
    /**
     * 顶部统计点击事件
     * @param {String} title 标题
     */
    onStatopClick (title) {
      switch (title) {
        case '当日警情数':
          this.situationVisible = true;
          break;
        case '平均处警时长':
          this.dealsituaVisible = true;
          break;
        case '实时警情数':
          this.realtimealertnumVisible = true;
          break;
        case '实时线路考勤':
          this.datacheckVisible = true;
          break;
        case '实时人员考勤':
          this.punchVisible = true;
          break;
        default:
          break;
      }
    },
    /**
     * 点击各个板块的标题，查看更多
     * @param {String} name 标题
     */
    onTitleClick (name) {
      if (name.includes('快警平台信息')) {
        name = 1;
      }
      switch (name) {
        case 1:
          this.setFullmap();
          break;
        case '大队值班领导':
          this.dutyleaderVisible = true;
          break;
        default:
          break;
      }
    },
    /**
     * 设置地图的全屏
     */
    setFullmap () {
      this.platformId = '';
      this.platformTitle = '';
      this.inputCar = '';
      this.zoom = 14;
      this.map.setZoom(14);
      this.panTo(111.48, 27.229);
      this.showLabels = _.cloneDeep(this.labels);
      // 辖区
      this.getJurisdiction();
      // 车辆
      this.getCurrentMarkers();
    },
    /**
     * 处理当前显示的marker
     * @param {Boolean} locate 是否定位到查询到的第一个点
     */
    getCurrentMarkers (locate = false, zoom) {
      let nameMarkers = []; // marker过滤
      this.markers.map(item => {
        item.isLocked = false;
      });
      if (this.platformTitle) {
        let parentName = this.platformTitle.split('-')[0];
        let name = this.platformTitle.split('-')[1];
        let pName = parentName.slice(0, parentName.length - 1);
        nameMarkers = this.markers.filter(item => {
          let wholeName = item.wholeName;
          return wholeName.includes(pName) && wholeName.includes(name);
        });
      }
      let cloneMarkers = [];
      if (nameMarkers.length) {
        cloneMarkers = _.cloneDeep(nameMarkers);
      } else {
        cloneMarkers = _.cloneDeep(this.markers);
      }
      // 关键字过滤
      if (this.inputCar) {
        cloneMarkers = cloneMarkers.filter(item => {
          let inputCar = this.inputCar;
          let carCode = item.carCode;
          let deptName = item.deptName;
          return (
            (carCode ? carCode.includes(inputCar) : '') ||
            (deptName ? deptName.includes(inputCar) : '')
          );
        });
      }
      let showMarkerArray = []; // 存储展示的车辆数组
      cloneMarkers.forEach(item => {
        // 平台车
        if (this.platformCar && item.type === '1') {
          showMarkerArray.push(item);
        }
        // 警车
        if (this.policeCar && item.type === '2') {
          showMarkerArray.push(item);
        }
        // 摩托车
        if (this.motorcycle && item.type === '3') {
          showMarkerArray.push(item);
        }
      });
      this.actualMarkers = showMarkerArray;
      if (zoom) {
        this.zoom = zoom;
      } else {
        this.showMarkers = this.queryInRect(showMarkerArray);
        if (
          locate &&
          this.showMarkers.length &&
          (!this.showPolygons.length || this.inputCar)
        ) {
          this.zoom = 18;
          this.panTo(this.showMarkers[0].lng, this.showMarkers[0].lat);
        }
      }
    },
    /**
     * 辖区展示切换
     * @param {Boolean} val 是否选中
     */
    getJurisdiction () {
      if (this.jurisdiction) {
        // 有辖区
        if (this.platformTitle) {
          // 有平台
          let parentName = this.platformTitle.split('-')[0];
          let name = this.platformTitle.split('-')[1];
          let pName = parentName.slice(0, parentName.length - 1);
          this.showPolygons = this.polygons.filter(item => {
            let polyName = item.name;
            return polyName.includes(pName) && polyName.includes(name);
          });
          if (this.showPolygons.length) {
            // 先定位到区域
            this.zoom = 14;
            this.panTo(
              this.showPolygons[0].coordList[0].lng,
              this.showPolygons[0].coordList[0].lat
            );
          }
        } else {
          this.showPolygons = _.cloneDeep(this.polygons);
        }
      } else {
        this.showPolygons = [];
      }
    },
    /**
     * 根据车牌号/平台车名查询某个车辆
     */
    getCarNoPanTo () {
      if (this.inputCar) {
        this.getCurrentMarkers(false, 9);
      } else {
        this.$message.info('请输入查询的车牌号或者平台车名');
      }
    },
    /**
     * 关闭大队值班领导的弹出框
     */
    onDutyleaderClose () {
      this.dutyleaderVisible = false;
    },
    /**
     * 获取快警平台信息
     */
    findPlatform () {
      this.$api.screen.findPlatform().then(res => {
        if (res.data.code === 0) {
          this.platformList = res.data.data;
        }
      });
    },
    /**
     * 打开快警平台弹出框
     * @param {String} row 平台数据
     */
    onPoliceOpen (row) {
      this.policeVisible = true;
      const { id, parentName, name } = row;
      this.platformId = id;
      this.platformTitle = parentName + '-' + name;
    },
    /**
     * 只展示某个平台的地图信息
     * @param {String} row 平台数据
     */
    onPlatformShow (row) {
      const { id, parentName, name } = row;
      this.platformId = id;
      this.platformTitle = parentName + '-' + name;
      let pName = parentName.slice(0, parentName.length - 1);
      // 网格
      this.getJurisdiction();
      // 车辆
      this.getCurrentMarkers(true);
      // 网格标签
      this.showLabels = this.labels.filter(item => {
        let labelName = item.name;
        return labelName.includes(pName) && labelName.includes(name);
      });
    },
    /**
     * 关闭快警平台弹出框
     */
    onPoliceClose () {
      this.policeVisible = false;
      this.platformId = '';
      this.platformTitle = '';
    },
    /**
     * 当前选中用户的id
     * @param {String} userId 用户id
     * @param {String} type 操作类型
     */
    onUserClick (userId, type, name) {
      this.currentUserId = userId;
      this.currentUserName = name;
      switch (type) {
        case 'look':
          this.userdetailVisible = true;
          break;
        case 'record':
          this.recordlistVisible = true;
          break;
        case 'task':
          this.patroltaskVisible = true;
          break;
        case 'track':
          this.currentDevId = ''; // 人员入口，车辆设备号为空
          this.currentDevType = '';
          this.currentDevName = '';
          this.maptrackVisible = true;
          break;
        default:
          break;
      }
    },
    /**
     * 根据车辆查询轨迹
     * @param {String} devid 车辆id
     */
    onCarTrack (devid, devName, devType) {
      this.currentDevId = devid;
      this.currentDevType = devType;
      this.currentDevName = devName;
      this.currentUserId = '';
      this.currentUserName = '';
      this.maptrackVisible = true;
    },
    /**
     * 当日警情数关闭
     */
    onSituationClose () {
      this.situationVisible = false;
      this.todayAlert();
    },
    /**
     * 平均处警时长关闭
     */
    onDealsituaClose () {
      this.dealsituaVisible = false;
      this.avgDealAlertTime();
    },
    /**
     * 实时警情数关闭
     */
    onRealtimealertClose () {
      this.realtimealertnumVisible = false;
      this.alarmList();
    },
    /**
     * 关闭用户详情弹出框
     */
    onUserdetailClose () {
      this.currentUserId = '';
      this.userdetailVisible = false;
    },
    /**
     * 关闭未打卡数据弹出框
     */
    onPunchClose () {
      this.punchVisible = false;
      this.getPunchData();
    },
    /**
     * 关闭数据考核弹出框
     */
    onDatacheckClose () {
      this.datacheckVisible = false;
      this.getCheckData();
    },
    /**
     * 实时警情
     */
    intime () {
      this.$api.screen.intime({ time: 5 }).then(res => {
        if (res.data.data) {
          this.realtimeAlertList = res.data.data;
        }
      });
    },
    /**
     * 当日警情数
     */
    todayAlert () {
      this.$api.screen.todayAlert().then(res => {
        if (res.data.data) {
          this.headStaData[0].value = res.data.data.allCount;
        }
      });
    },
    /**
     * 平均处警时长
     */
    avgDealAlertTime () {
      this.$api.screen.avgDealAlertTime().then(res => {
        if (res.data.data) {
          this.headStaData[1].value = formateTime(res.data.data.useTime);
        }
      });
    },
    // 实时警情数及明细
    alarmList () {
      this.$api.screen.alarmList().then(res => {
        if (res.data.data) {
          this.headStaData[2].value = res.data.data.total;
        }
      });
    },
    /**
     * 获取数据考核的总数-实时线路考勤
     */
    getCheckData () {
      let params = {
        startDate: this.todayDate,
        endDate: this.todayDate,
        pageSize: 10,
        pageNumber: 1
      };
      this.$api.screen.checkList(params).then(res => {
        this.headStaData[3].value = res.data.total;
      });
    },
    /**
     * 获取未打卡数据的总数-实时人员考勤
     */
    getPunchData () {
      let params = {
        userId: '',
        pageSize: 10,
        pageNumber: 1,
        name: '',
        state: 1,
        date: this.todayDate
      };
      this.$api.screen.userAllClockedList(params).then(res => {
        this.headStaData[4].value = res.data.total;
      });
    },
    /**
     * 平台巡逻排名
     */
    carRankList () {
      this.$api.screen.carRankList().then(res => {
        if (res.data.data) {
          this.patrolrankList = res.data.data;
        }
      });
    },
    /**
     * 统计图一
     */
    getChartOne () {
      if (this.oneChartBar) {
        this.oneChartBar.clear();
      }
      this.$api.screen.dataCompareByYearMonth().then(res => {
        const {
          data: { data }
        } = res;
        let legendData = data.map(item => item.name);
        let seriesData = data.map(item => {
          let obj = {};
          obj.name = item.name;
          obj.type = 'bar';
          obj.data = item.data;
          return obj;
        });
        let option = {
          title: {
            text: '处警统计',
            left: 'center',
            textStyle: {
              color: '#fff'
            }
          },
          grid: {
            top: 40,
            right: 40,
            left: 60
          },
          tooltip: {
            trigger: 'axis'
          },
          legend: {
            bottom: 8,
            data: legendData,
            textStyle: {
              color: '#fff'
            }
          },
          color: [
            '#3548AE',
            '#1D6FBB',
            '#714FE2',
            '#CA60E4',
            '#11AFEF',
            '#B06E2E',
            '#F269A0',
            '#F96340',
            '#3EC326',
            '#F7FE8A'
          ],
          toolbox: {
            show: true,
            feature: {
              magicType: { show: true, type: ['line', 'bar'] },
              restore: { show: true },
              saveAsImage: { show: true }
            }
          },
          calculable: true,
          xAxis: [
            {
              type: 'category',
              axisLine: {
                lineStyle: {
                  color: '#ccc'
                }
              },
              data: Object.keys(this.monthArray)
            }
          ],
          yAxis: [
            {
              type: 'value',
              axisLine: {
                lineStyle: {
                  color: '#ccc'
                }
              }
            }
          ],
          series: seriesData
        };
        this.oneChartBar.setOption(option);
        this.oneChartBar.off('click');
        this.oneChartBar.on('click', params => {
          const { seriesName: year, name, color } = params;
          this.chartBackOne = true;
          this.getChartOneYearMonth(year, this.monthArray[name], color);
        });
      });
    },
    /**
     * 统计图一详情
     */
    getChartOneYearMonth (year, month, color) {
      this.secondYear = year;
      this.secondMonth = month;
      if (this.oneChartBar) {
        this.oneChartBar.clear();
      }
      this.$api.screen.dataCompareByYearMonth2({ year, month }).then(res => {
        const {
          data: { data }
        } = res;
        let legendData = {
          data: ['快警数量']
        };
        let seriesData = [{
          name: '快警数量',
          data: data.data,
          type: 'bar'
        }];
        let option = {
          title: {
            text: `${year}年${month}月每日快警汇总数据统计`,
            left: 'center',
            textStyle: {
              color: '#fff'
            }
          },
          grid: {
            top: 40,
            right: 40,
            left: 60
          },
          tooltip: {
            trigger: 'axis'
          },
          legend: {
            bottom: 8,
            data: legendData,
            textStyle: {
              color: '#fff'
            }
          },
          color,
          toolbox: {
            show: true,
            feature: {
              magicType: { show: true, type: ['line', 'bar'] },
              restore: { show: true },
              saveAsImage: { show: true }
            }
          },
          calculable: true,
          xAxis: {
            type: 'category',
            axisLine: {
              lineStyle: {
                color: '#ccc'
              }
            },
            axisLabel: {
              interval: 0,
              rotate: 30
            },
            data: data.dateList
          },
          yAxis: [
            {
              type: 'value',
              axisLine: {
                lineStyle: {
                  color: '#ccc'
                }
              }
            }
          ],
          series: seriesData
        };
        this.oneChartBar.setOption(option);
        this.oneChartBar.off('click');
      });
    },
    /**
     * 统计图二
     */
    getChartTwo () {
      if (this.twoChartBar) {
        this.twoChartBar.clear();
      }
      this.$api.screen.dataCompareByYearMonthType().then(res => {
        const {
          data: { data }
        } = res;
        let legendData = data.map(item => {
          if (item.name !== 'time') return item.name;
        });
        let xData = [];
        let seriesData = data.map(item => {
          if (item.name !== 'time') {
            let obj = {};
            obj.name = item.name;
            obj.type = 'bar';
            obj.data = item.data;
            return obj;
          } else {
            xData = item.dateList;
          }
        });
        let option = {
          title: {
            text: '警情',
            left: 'center',
            textStyle: {
              color: '#fff'
            }
          },
          grid: {
            top: 40,
            right: 40,
            left: 82,
            bottom: 80
          },
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'shadow'
            }
          },
          legend: {
            bottom: 28,
            data: legendData,
            textStyle: {
              color: '#fff'
            }
          },
          color: [
            '#003366',
            '#006699',
            '#4cabce',
            '#e5323e',
            '#3548AE',
            '#1D6FBB',
            '#714FE2',
            '#CA60E4',
            '#11AFEF',
            '#B06E2E',
            '#F269A0',
            '#F96340',
            '#3EC326',
            '#F7FE8A'
          ],
          toolbox: {
            show: true,
            feature: {
              magicType: { show: true, type: ['line', 'bar'] },
              restore: { show: true },
              saveAsImage: { show: true }
            }
          },
          xAxis: {
            type: 'category',
            data: xData,
            axisLine: {
              lineStyle: {
                color: '#ccc'
              }
            },
            triggerEvent: true
          },
          yAxis: {
            type: 'value',
            axisLine: {
              lineStyle: {
                color: '#ccc'
              }
            }
          },
          series: seriesData
        };
        this.twoChartBar.setOption(option);
        this.twoChartBar.off('click');
        this.twoChartBar.on('click', params => {
          const { componentType, name, value } = params;
          if (componentType === 'series' || componentType === 'xAxis') {
            let year = '';
            if (componentType === 'series') {
              year = name;
            } else {
              year = value;
            }
            this.chartBack = 'second';
            this.chartTwoYear = year;
            this.getChartTwoMonth(this.chartTwoYear);
          }
        });
      });
    },
    /**
     * 统计图二详情
     */
    getChartTwoMonth (year) {
      if (this.twoChartBar) {
        this.twoChartBar.clear();
      }
      this.$api.screen.dataCompareByYearMonthType2({ year }).then(res => {
        const {
          data: { data }
        } = res;
        let legendData = data.map(item => item.name);
        let seriesData = [];
        let typeObj = {};
        data.forEach(item => {
          let { name, data, type } = item;
          let obj = {
            name,
            data,
            type: 'bar'
          };
          let obj1 = {
            [name]: type
          }
          seriesData.push(obj);
          typeObj = {
            ...typeObj,
            ...obj1
          }
        })
        let option = {
          title: {
            text: `${year}年警情`,
            left: 'center',
            textStyle: {
              color: '#fff'
            }
          },
          grid: {
            top: 40,
            right: 40,
            left: 82,
            bottom: 80
          },
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'shadow'
            }
          },
          legend: {
            bottom: 28,
            data: legendData,
            textStyle: {
              color: '#fff'
            }
          },
          color: [
            '#003366',
            '#006699',
            '#4cabce',
            '#e5323e',
            '#3548AE',
            '#1D6FBB',
            '#714FE2',
            '#CA60E4',
            '#11AFEF',
            '#B06E2E',
            '#F269A0',
            '#F96340',
            '#3EC326',
            '#F7FE8A'
          ],
          toolbox: {
            show: true,
            feature: {
              magicType: { show: true, type: ['line', 'bar'] },
              restore: { show: true },
              saveAsImage: { show: true }
            }
          },
          xAxis: {
            type: 'category',
            data: Object.keys(this.monthArray),
            axisLine: {
              lineStyle: {
                color: '#ccc'
              }
            }
          },
          yAxis: {
            type: 'value',
            axisLine: {
              lineStyle: {
                color: '#ccc'
              }
            }
          },
          series: seriesData
        };
        this.twoChartBar.setOption(option);
        this.twoChartBar.off('click');
        this.twoChartBar.on('click', params => {
          const { seriesName, name, color } = params;
          this.chartBack = 'third';
          this.getChartTwoYearMonth(year, this.monthArray[name], color, typeObj[seriesName], seriesName);
        });
      });
    },
    /**
     * 统计图二详情-详情
     */
    getChartTwoYearMonth (year, month, color, type, seriesName) {
      this.thirdYear = year;
      this.thirdMonth = month;
      this.thirdType = type;
      this.thirdName = `${year}年${month}月每日${seriesName}数据统计`;
      if (this.twoChartBar) {
        this.twoChartBar.clear();
      }
      this.$api.screen.dataCompareByYearMonthType3({ year, month, type }).then(res => {
        const {
          data: { data }
        } = res;
        let legendData = {
          data: ['案件数量']
        };
        let seriesData = [{
          name: '案件数量',
          data: data.data,
          type: 'bar'
        }];
        let option = {
          title: {
            text: this.thirdName,
            left: 'center',
            textStyle: {
              color: '#fff'
            }
          },
          grid: {
            top: 40,
            right: 40,
            left: 60
          },
          tooltip: {
            trigger: 'axis'
          },
          legend: {
            bottom: 8,
            data: legendData,
            textStyle: {
              color: '#fff'
            }
          },
          color,
          toolbox: {
            show: true,
            feature: {
              magicType: { show: true, type: ['line', 'bar'] },
              restore: { show: true },
              saveAsImage: { show: true }
            }
          },
          calculable: true,
          xAxis: {
            type: 'category',
            axisLine: {
              lineStyle: {
                color: '#ccc'
              }
            },
            axisLabel: {
              interval: 0,
              rotate: 30
            },
            data: data.dateList
          },
          yAxis: [
            {
              type: 'value',
              axisLine: {
                lineStyle: {
                  color: '#ccc'
                }
              }
            }
          ],
          series: seriesData
        };
        this.twoChartBar.setOption(option);
        this.twoChartBar.off('click');
      });
    },
    /**
     * 返回统计图一
     */
    onChartBackOne () {
      this.chartBackOne = false;
      this.getChartOne();
    },
    /**
     * 返回统计图二
     */
    onChartBack () {
      if (this.chartBack === 'third') {
        this.chartBack = 'second';
        this.getChartTwoMonth(this.chartTwoYear);
      } else if (this.chartBack === 'second') {
        this.chartBack = 'first';
        this.getChartTwo();
      }
    },
    /**
     * 下载统计图一表格
     */
    onOnechartDown () {
      this.$confirm('是否进行每日快警汇总数据统计表格下载?', '提示', {
        type: 'info'
      })
        .then(() => {
          window.open(`${this.localUrl}/police/exportExcel?year=${this.secondYear}&month=${this.secondMonth}`);
        })
    },
    /**
     * 下载统计图二表格
     */
    onTwochartDown () {
      this.$confirm(`是否进行${this.thirdName}表格下载?`, '提示', {
        type: 'info'
      })
        .then(() => {
          window.open(`${this.localUrl}/police/exportExcel?year=${this.thirdYear}&month=${this.thirdMonth}&type=${this.thirdType}`);
        })
    },
    /**
     * 地图点击事件，如果有弹出框，则关闭
     */
    mapClick () {
      if (this.lastInfoBox) {
        this.lastInfoBox.close();
      }
    },
    queryInRect (points) {
      if (this.map) {
        this.zoom = this.map.getZoom(); // 当前缩放级别
        if (
          !(
            this.showMarkers &&
            this.showMarkers.length === 1 &&
            this.showMarkers[0].isLocked
          ) &&
          this.zoom !== 19
        ) {
          if (!points) {
            points = this.actualMarkers;
          }
          let cp = this.map.getBounds(); // 返回map可视区域，以地理坐标表示
          let sw = cp.getSouthWest(); // 获取西南角的经纬度(左下端点)
          let ne = cp.getNorthEast(); // 获取东北角的经纬度(右上端点)
          let wn = new BMap.Point(sw.lng, ne.lat); // 获取西北角的经纬度(左上端点)
          this.zoom = this.map.getZoom(); // 当前缩放级别
          let es = new BMap.Point(ne.lng, sw.lat); // 获取东南角的经纬度(右下端点)
          let polygon = new BMap.Polygon([
            new BMap.Point(sw.lng, sw.lat), // 左下
            new BMap.Point(wn.lng, wn.lat), // 左上
            new BMap.Point(ne.lng, ne.lat), // 右上
            new BMap.Point(es.lng, es.lat) // 右下
          ]);
          let currShowCount = 0; // 本次拖动或缩放已显示的点数
          let markerArr = [];
          points.forEach(item => {
            let { lng, lat } = item;
            let point = new BMap.Point(lng, lat);
            if (BMapLib.GeoUtils.isPointInPolygon(point, polygon)) {
              if (this.zoom <= 10 || currShowCount <= 100) {
                // 放大到最大层数后，则显示当前可视区内所有点，鉴于层级较大显示的摄像头较少，因此不会出现卡顿情况
                markerArr.push(item);
                currShowCount++; // 本次已显示数加1
              }
            }
          });
          this.showMarkers = markerArr;
          return this.showMarkers;
        }
      }
    },
    getLoginCode () {
      return new Promise((resolve, reject) => {
        this.$api.screen
          .getLoginCode()
          .then(res => {
            if (res.data.code === 0) {
              resolve(res.data.message);
            } else {
              window.location.href = 'http://218.76.207.66:8019/admin/login';
            }
          })
          .catch(err => reject(err));
      });
    },
    /**
     * 获取登录的code
     */
    compareLoginCode () {
      let urlParams = {}
      window.location.href.replace(/([^?&=]+)=([^?&=]*)/ig, (a, b, c) => {
        urlParams[b] = c
      })
      let urlCode = urlParams['code'];
      if (urlCode === 'c13f06c29c744308a7c1b7fe4f94dd4dbac2812cbbbe4fadb826d32f0bf94e36') {
        this.initRender();
      } else {
        let _url = 'http://218.76.207.66:8019/admin/login';
        if (this.interStatus === 'inside') {
          _url = 'http://65.96.2.127:8019/admin/login';
        }
        window.location.href = _url;
      }

      // this.getLoginCode().then(res => {
      //   console.log(res);
      //   let code = res;
      //   alert(code);
      //   alert(urlCode === code);
      //   if (urlCode === code) {
      //     this.initRender();
      //   } else {
      //     window.location.href = 'http://218.76.207.66:8019/admin/login';
      //   }
      // });
    },
    initRender () {
      let today = new Date();
      let month = today.getMonth() + 1;
      let day = today.getDate();
      this.todayDate =
        today.getFullYear() +
        '-' +
        (month < 10 ? '0' + month : month) +
        '-' +
        (day < 10 ? '0' + day : day);
      this.findPlatform(); // 获取快警平台信息
      this.todayAlert(); // 当日警情数
      this.avgDealAlertTime(); // 平均处警时长
      this.alarmList(); // 实时警情数及明细
      this.getPunchData(); // 获取未打卡数据总数
      this.getCheckData(); // 获取数据考核总数
      this.carRankList(); // 获取巡逻排名
      this.intime(); // 实行警情
      this.realtimeAlertInter = setInterval(this.intime, 300000);
    }
  }
};

export default basicScreen;
