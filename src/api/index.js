/*
 * @Author: gyp
 * @Date: 2020-03-17 09:42:11
 * @LastEditors: gyp
 * @LastEditTime: 2020-09-08 17:15:18
 * @Description: 如果分模块，这里统一暴露出需要的模块
 * @FilePath: \sy_kjxc_web\src\api\index.js
 */
import { patrolPoint, clockinRecord } from '@/api/patrolManage/index.js';
// import screen from '@/api/screen/screen.js'; // 外网
import screen from '@/api/screen/screen_inside.js'; // 内网
export {
  patrolPoint,
  clockinRecord,
  screen
}
