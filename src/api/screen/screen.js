/*
 * @Author: gyp
 * @Date: 2020-05-09 09:52:32
 * @LastEditors: gyp
 * @LastEditTime: 2020-06-19 14:51:40
 * @Description: 大屏接口
 * @FilePath: \sy_kjxc_web\src\api\screen\screen.js
 */

import request from '../request.js'
let serverUrl = 'http://218.76.207.66:8030';
// const localUrl = 'http://localhost:8030';
const localUrl = 'http://218.76.207.66:8030';
const screen = {
  // 查询全部接口
  getPoliceCarInit () {
    return request.get(serverUrl + '/patrolCar/getPoliceCarInit')
  },
  // 获取快警平台信息
  findPlatform () {
    return request.get(serverUrl + '/platformUser/findPlatform')
  },
  // 根据平台id查询所有小组
  findByPlatformId (params) {
    return request.get(serverUrl + '/platformUser/findByPlatformId', { params })
  },
  // 根据平台id查询人员信息
  findUserVoByPlatformId (params) {
    return request.get(serverUrl + '/platformUser/findUserVoByPlatformId', { params })
  },
  // 根据平台id查询设备信息
  findEquipmentByParam (params) {
    return request.get(serverUrl + '/platformUser/findEquipmentByParam', { params })
  },
  // 大队值班领导信息
  findUserByIdentity (params) {
    return request.get(serverUrl + '/platformUser/findUserByIdentity', { params })
  },
  // 查询用户详细信息
  findUserDetails (params) {
    return request.get(serverUrl + '/platformUser/findUserDetails', { params })
  },
  // 获取用户打卡记录
  userAllClockedList (params) {
    return request.get(serverUrl + '/user/userAllClockedList', { params })
  },
  // 获取用户任务
  userSpotList (params) {
    return request.get(serverUrl + '/user/userSpotList', { params })
  },
  // 获取数据考核
  checkList (params) {
    return request.get(serverUrl + '/platform/list', { params })
  },
  // 实时警情
  intime (params) {
    return request.get(localUrl + '/police/intime', { params });
  },
  // 当日警情数
  todayAlert () {
    return request.get(localUrl + '/police/todayAlert');
  },
  // 平均处警时长
  avgDealAlertTime (params) {
    return request.get(localUrl + '/police/avgDealAlertTime', { params });
  },
  // 平台巡逻排名
  carRankList () {
    return request.get(localUrl + '/police/carRankList');
  },
  // 处警统计数据对比 按年月（内网）
  dataCompareByYearMonth () {
    return request.get(localUrl + '/police/dataCompareByYearMonth');
  },
  // 警情类型数据对比 按年（内网）
  dataCompareByYearMonthType (params) {
    return request.get(localUrl + '/police/dataCompareByYearMonthType', { params });
  },
  // 实时警情数及明细
  alarmList (params) {
    return request.get(localUrl + '/police/alarmList', { params });
  },
  // 车辆轨迹
  carHistory (params) {
    return request.get(localUrl + '/car/history', { params });
  },
  // 巡逻点
  spotList (params) {
    return request.get(serverUrl + '/platform/spotList', { params });
  },
  // 获取平台已打卡明细
  hasClockedList (params) {
    return request.get(serverUrl + '/platform/hasClockedList', { params });
  },
  // 获取平台未打卡明细
  noClockedList (params) {
    return request.get(serverUrl + '/platform/noClockedList', { params });
  },
  // 获取平台处警明细
  dealproblemsList (params) {
    return request.get(serverUrl + '/platform/dealproblemsList', { params });
  },
  // 添加巡检抽查
  applyPatrolCheck (params) {
    return request.post(serverUrl + '/patrolCheck/applyPatrolCheck', params);
  },
  // 获取code
  getLoginCode () {
    return request.get(serverUrl + '/police/code');
  }
}
export default screen
