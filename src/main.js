/*
 * @Author: gyp
 * @Date: 2020-03-17 09:42:11
 * @LastEditors: gyp
 * @LastEditTime: 2020-07-01 19:04:55
 * @Description: 入口文件
 * @FilePath: \sy_kjxc_web\src\main.js
 */
import 'babel-polyfill'
import { setRem } from './utils/rem'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
// import store from './store'
import * as api from './api'
import './assets/css/base.less'
import './plugins/element.js'
import * as filters from './filters' // global filters
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import echarts from 'echarts'
import VueCookies from 'vue-cookies'
import './icons' // icon
import './styles/index.less'
import '@/assets/font/iconfont/iconfont.css'
import Qs from 'qs';

Vue.prototype.$api = api // 注册为全局指令
Vue.prototype.$echarts = echarts
Vue.prototype.$qs = Qs;
Vue.use(VueCookies)
Vue.use(iView)

const components = [];
components.forEach((v, i) => {
  Object.keys(v).forEach(v => {
    Vue.component(v, components[i][v])
  })
})

setRem()

Vue.prototype.isEvenOrOdd = function (index) {
  if (index % 2 === 0) {
    return ''
  } else {
    return 'isOdd'
  }
}

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
